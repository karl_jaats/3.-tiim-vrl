package ee.ut.cs.wad.raamatukogu.wishedbook;

import ee.ut.cs.wad.raamatukogu.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WishedBookRepository extends JpaRepository<WishedBook, Long> {
    List<WishedBook> findAllByUser(@Param("user") User user);
}
