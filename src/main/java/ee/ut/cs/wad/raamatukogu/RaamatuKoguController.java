package ee.ut.cs.wad.raamatukogu;

import ee.ut.cs.wad.raamatukogu.user.User;
import ee.ut.cs.wad.raamatukogu.user.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class RaamatuKoguController {

    private final static String RAAMATUKOGU_PAGE = "pealeht";

    @GetMapping("/raamat")
    public String success(Model model){
        return RAAMATUKOGU_PAGE;
    }
    @GetMapping("/registreerimine")
    public String goToRegistration(Model model){
        return "registreerimine";
    }
    @GetMapping("/")
    public String redirect(Model model){
        return RAAMATUKOGU_PAGE;
    }
}






