package ee.ut.cs.wad.raamatukogu.wishedbook;

import ee.ut.cs.wad.raamatukogu.book.Book;
import ee.ut.cs.wad.raamatukogu.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="wishedBook")
public class WishedBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long wishedBookId;

    @ManyToOne
    @JoinColumn(name="bookId")
    private Book book;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;
}
