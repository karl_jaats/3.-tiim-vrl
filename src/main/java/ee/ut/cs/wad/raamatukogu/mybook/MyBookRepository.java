package ee.ut.cs.wad.raamatukogu.mybook;

import ee.ut.cs.wad.raamatukogu.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MyBookRepository extends JpaRepository<MyBook, Long> {
    List<MyBook> findAllByUser(@Param("user") User user);
}
