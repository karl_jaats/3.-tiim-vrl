package ee.ut.cs.wad.raamatukogu.wishedbook;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WishedBookDTO {

    private Long bookId;
    private Long userId;
}
