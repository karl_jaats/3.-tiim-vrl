package ee.ut.cs.wad.raamatukogu.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmail(@Param("email") String email);
}
