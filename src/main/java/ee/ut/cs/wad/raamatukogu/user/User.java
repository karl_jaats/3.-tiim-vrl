package ee.ut.cs.wad.raamatukogu.user;

import ee.ut.cs.wad.raamatukogu.mybook.MyBook;
import ee.ut.cs.wad.raamatukogu.wishedbook.WishedBook;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="\"user\"") // needs quotes as user is a reserved keyword
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String gender;
    private Date dateOfBirth;
    private String phoneNumber;

    @OneToMany(mappedBy = "user")
    private List<MyBook> myBooks;

    @OneToMany(mappedBy = "user")
    private List<WishedBook> wishedBooks;
}
