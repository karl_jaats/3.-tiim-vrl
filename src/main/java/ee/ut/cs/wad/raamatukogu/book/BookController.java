package ee.ut.cs.wad.raamatukogu.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/book")
public class BookController {

    private final BookService bookService;

    @Autowired
    BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/add")
    public String addNewBook(@ModelAttribute BookDTO bookDTO, Model model) {
        Book book = new Book();
        book.setAuthor(bookDTO.getAuthor());
        book.setTitle(bookDTO.getTitle());
        book.setYear(bookDTO.getYear());

        try {
            bookService.addBook(book);
            model.addAttribute("successMessage", "Book added successfully!");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "success";
    }

    @GetMapping("/all")
    public String showAllBooks(Model model){
        return bookService.getAllBooks().toString();
    }
}
