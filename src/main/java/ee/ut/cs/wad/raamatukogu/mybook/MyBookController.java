package ee.ut.cs.wad.raamatukogu.mybook;

import ee.ut.cs.wad.raamatukogu.book.BookService;
import ee.ut.cs.wad.raamatukogu.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/myBook")
public class MyBookController {

    private final MyBookService myBookService;
    private final BookService bookService;
    private final UserService userService;

    @Autowired
    MyBookController(MyBookService myBookService, UserService userSerivice, BookService bookService) {
        this.myBookService = myBookService;
        this.userService = userSerivice;
        this.bookService = bookService;
    }

    @PostMapping("/add")
    public String addNewMyBook(@ModelAttribute MyBookDTO myBookDTO, Model model) {
        MyBook myBook = new MyBook();
        myBook.setBook(bookService.getBookById(myBookDTO.getBookId()));
        myBook.setUser(userService.getUserById(myBookDTO.getUserId()));

        try {
            myBookService.addMyBook(myBook);
            model.addAttribute("successMessage", "MyBook added successfully!");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "success";
    }
}
