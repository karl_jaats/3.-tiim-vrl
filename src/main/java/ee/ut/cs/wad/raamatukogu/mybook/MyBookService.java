package ee.ut.cs.wad.raamatukogu.mybook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyBookService {
    private final MyBookRepository myBookRepository;

    @Autowired
    MyBookService(MyBookRepository myBookRepository) {
        this.myBookRepository = myBookRepository;
    }

    public void addMyBook(MyBook myBook){
        myBookRepository.save(myBook);
    }
}
