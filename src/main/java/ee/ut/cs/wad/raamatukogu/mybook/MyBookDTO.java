package ee.ut.cs.wad.raamatukogu.mybook;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyBookDTO {

    private Long bookId;
    private Long userId;
}
