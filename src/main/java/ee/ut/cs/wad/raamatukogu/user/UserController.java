package ee.ut.cs.wad.raamatukogu.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/registreerimine")
public class UserController {

    private final static String REGISTRATION_PAGE = "registreerimine";
    private final UserService userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registreerimine")
    public String addNewUser(@ModelAttribute UserDTO userDTO, Model model) {
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setGender(userDTO.getGender());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        try {
            userService.addUser(user);
            model.addAttribute("successMessage", "User added successfully!");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }
        return REGISTRATION_PAGE;
    }

    @GetMapping("/registreerimine")
    public String showAllUsers(Model model){
        return userService.getAllUsers().toString();
    }
}
