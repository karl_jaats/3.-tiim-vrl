package ee.ut.cs.wad.raamatukogu.wishedbook;

import ee.ut.cs.wad.raamatukogu.book.BookService;
import ee.ut.cs.wad.raamatukogu.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/wishedBook")
public class WishedBookController {
    private final WishedBookService wishedBookService;
    private final BookService bookService;
    private final UserService userService;

    @Autowired
    WishedBookController(WishedBookService wishedBookService, UserService userSerivice, BookService bookService) {
        this.wishedBookService = wishedBookService;
        this.userService = userSerivice;
        this.bookService = bookService;
    }

    @PostMapping("/add")
    public String addNewMyBook(@ModelAttribute WishedBookDTO wishedBookDTO, Model model) {
        WishedBook wishedBook = new WishedBook();
        wishedBook.setBook(bookService.getBookById(wishedBookDTO.getBookId()));
        wishedBook.setUser(userService.getUserById(wishedBookDTO.getUserId()));

        try {
            wishedBookService.addWishedBook(wishedBook);
            model.addAttribute("successMessage", "MyBook added successfully!");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "success";
    }

}
