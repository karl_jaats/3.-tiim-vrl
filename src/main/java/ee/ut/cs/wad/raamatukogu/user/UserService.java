package ee.ut.cs.wad.raamatukogu.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        User existing = userRepository.findUserByEmail(user.getEmail());
        if (existing != null) {
            throw new UnsupportedOperationException("User with that email already exists.");
        }

        // INSERT INTO user (firstName, lastName, email, password, gender, dateOfBirth, phoneNumber)
        // VALUES (user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(),
        // user.getGender(), user.getDateOfBirth(), user.getPhoneNumber());
        userRepository.save(user);
    }

    public User getUserById(Long id){
        return userRepository.getOne(id);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
