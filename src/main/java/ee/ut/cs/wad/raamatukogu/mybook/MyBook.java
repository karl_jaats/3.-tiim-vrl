package ee.ut.cs.wad.raamatukogu.mybook;

import ee.ut.cs.wad.raamatukogu.book.Book;
import ee.ut.cs.wad.raamatukogu.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="myBook")
public class MyBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long myBookId;

    @ManyToOne
    @JoinColumn(name="bookId")
    private Book book;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;
}
