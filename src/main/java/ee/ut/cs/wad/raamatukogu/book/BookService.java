package ee.ut.cs.wad.raamatukogu.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void addBook(Book book){
        Book existing = bookRepository.findBook(book.getTitle(), book.getAuthor(), book.getYear());
        if (existing != null) {
            throw new UnsupportedOperationException("Such a book already exists.");
        }

        // INSERT INTO book (title, author, year) VALUES (book.getTitle(), book.getAuthor(), book.getYear());
        bookRepository.save(book);
    }

    public Book getBookById(Long id){
        return bookRepository.getOne(id);
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }
}
