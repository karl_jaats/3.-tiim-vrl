package ee.ut.cs.wad.raamatukogu.book;

import ee.ut.cs.wad.raamatukogu.mybook.MyBook;
import ee.ut.cs.wad.raamatukogu.wishedbook.WishedBook;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;

    private String title;
    private String author;
    private String year;

    @OneToMany(mappedBy = "book")
    private List<MyBook> myBooks;

    @OneToMany(mappedBy = "book")
    private List<WishedBook> wishedBooks;
}
