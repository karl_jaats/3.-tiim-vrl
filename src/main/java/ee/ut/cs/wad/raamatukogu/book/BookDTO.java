package ee.ut.cs.wad.raamatukogu.book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookDTO {
    private String title;
    private String author;
    private String year;
}
