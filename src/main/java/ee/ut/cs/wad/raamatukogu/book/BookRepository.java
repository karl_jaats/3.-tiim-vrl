package ee.ut.cs.wad.raamatukogu.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends JpaRepository<Book, Long> {

    Book findBookByTitle(@Param("title") String title);

    @Query(value = "SELECT * FROM book WHERE title = :title AND author = :author AND year = :year", nativeQuery = true)
    Book findBook(@Param("title") String title, @Param("author") String author, @Param("year") String year);
}
