package ee.ut.cs.wad.raamatukogu.wishedbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WishedBookService {
    private final WishedBookRepository wishedBookRepository;

    @Autowired
    WishedBookService(WishedBookRepository wishedBookRepository) {
        this.wishedBookRepository = wishedBookRepository;
    }

    public void addWishedBook(WishedBook wishedBook){
        wishedBookRepository.save(wishedBook);
    }
}
