# Team members #
Karl Jääts, Sten Teemant, Fredi Arro


## I etapp ##
* [Projektiplaan](https://bitbucket.org/karl_jaats/3.-tiim-vrl/wiki/Projektiplaan)
* [Prototüüp](https://drive.google.com/open?id=1KXn20jIrRCHGkD_PPGX0cpfYYY7IGQCy)

## II etapp ##
* [Heroku testkeskond](https://aqueous-caverns-82234.herokuapp.com/raamat)

## III etapp ##
* Veebilehel sisu esitamine kaardil

## IV etapp ##
Ei teinud midagi

## V etapp ##
Ei teinud midagi

## VI etapp ##
Ei teinud midagi

## VII etapp ##
* Andmete salvestamine andmebaasi